#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"

#define BUFSIZE 256
#define PIPESENDBUF 128
#define ASSERT_LOG(logfile, str, ...) if (logfile != NULL) fprintf(logfile, str, __VA_ARGS__)

void yesno(FILE *fd, char *buf) {
    char ch;
    while ((ch = fgetc(fd)) != EOF)
        putchar(ch);
    putchar('\n');
    fclose(fd);
    do {
        printf("\x1B[31mIs this the file you want to submit? (y/n): \x1B[0m");
        buf[BUFSIZE - 1] = '\0'; /* '\0' set to be able to verify input size compared to BUFSIZE */ 
        fgets(buf, BUFSIZE, stdin);
        if (buf[BUFSIZE - 1] != '\0') {
            do {
                buf[BUFSIZE - 1] = '\0';
                fgets(buf, BUFSIZE, stdin);
            } while (buf[BUFSIZE - 1] != '\0'); /* repeat to consume the input */
            continue;
        }
    } while (strcmp(buf, "y\n") && strcmp(buf, "yes\n") && strcmp(buf, "no\n") && strcmp(buf, "n\n"));
    if (buf[0] == 'n') {
        printf("OK, exiting now..\n");
        exit(EXIT_FAILURE);
    }
    return;
}

int pipe_write(int fd, void *buf, size_t size){
    int sent, n;
    for (sent = 0; sent < size; sent +=n){
        if ((n = write(fd, buf + sent, size - sent)) == -1)
            return -1;
    }
    return sent;
}

int send_file(FILE *from, int dest){
    size_t i;
    int cnt;
    char buf[PIPESENDBUF];
    cnt = 0;
    while ((i = fread(buf, 1, PIPESENDBUF, from))){
        //including zero in the case of fread returning -1 (-1 must be returned from this function aswell)
        if (pipe_write(dest, buf, i) < 1)
            return -1;
        cnt += i;
    }
    return cnt;
}

int main(int argc, char *argv[]) {
    FILE *fd, *log_file;
    char ch;
    char buf[BUFSIZE];
    struct passwd *pw;
    char *username;
    int file_feed[2];
    if (argc != 2) {
        char *prog_name = strrchr(argv[0], '/');
        if (prog_name++ == NULL)
            prog_name = argv[0];
        fprintf(stderr, "Run program as: %s FILE\nFILE: the file you want to submit\n", prog_name);
        return EXIT_FAILURE;
    }
    pw = getpwuid(geteuid());
    if (pw == NULL){
        fprintf(stderr, "Unable to determine your username, what are you up to? \n");
        return EXIT_FAILURE;
    }
    username = pw->pw_name;
    log_file = NULL;
    if ((fd = fopen(argv[1], "r")) == NULL) {
        fprintf(stderr, "Unable to open \"%s\"\nAre you sure that's the correct filename? Try again..\n", argv[1]);
        ASSERT_LOG(log_file, "[FILE NOT FOUND] <%s> : %s\n", username, argv[1]);
        return EXIT_FAILURE;
    }
    ASSERT_LOG(log_file, "[USER SENDING FILE] <%s> : %s\n", username, argv[1]);
    pipe(file_feed);
    if (fork()) {
        /* CHILD */
        char *filename = strrchr(argv[1], '/');
        if (filename++ == NULL)
            filename = argv[1];
        dup2(file_feed[0], 0);
        close(file_feed[0]);
        close(file_feed[1]);
        execl(SUBMIT_ACTUAL_FILENAME, "submit_actual", username, filename, NULL);
        fprintf(stderr, "Failed to send file, please contact admin.\n");
        ASSERT_LOG(log_file, "[EXEC ERROR] <%s> : Unable to exec submit_actual, errno %d\n", username, errno);
        return EXIT_FAILURE;
    }
    else {
        /* FATHER */
        close(file_feed[0]);
        if ((fd = fopen(argv[1], "r")) == NULL) {
            fprintf(stderr, "Unable to open \"%s\" for reading..\n", argv[1]);
            ASSERT_LOG(log_file, "[FILE OPEN ERROR] <%s> : Unable to open file %s, errno %d\n", username, argv[1], errno);
            return EXIT_FAILURE;
        }
        send_file(fd, file_feed[1]);
        close(file_feed[1]);
    }
    ASSERT_LOG(log_file, "[USER SENT FILE] <%s> : %s\n", username, argv[1]);
    return EXIT_SUCCESS;
}
