all: submit_actual submit_exec

submit_actual: submit_actual.c config.h
	gcc -o $@ $< 

submit_exec: submit_exec.c config.h
	gcc -o $@ $< 

clean:
	@rm submit_actual submit_exec 2> /dev/null
