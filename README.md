### Unrefined homework submitter ###

* Configure directory and program locations by editing *config.h*
* Create submissions directory as per the location in *config.h*
* Use **make** to compile
* Set the SUID bit on *submit\_actual*

```
#!bash
    chmod 4755 submit_actual
```



**IMPORTANT NOTE:** This program has not been tested for security.  An attacker may be able to exploit the submit\_actual program to gain access to a shell owned by you. Use at your own risk.