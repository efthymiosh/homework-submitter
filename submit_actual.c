#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "config.h"

#define MAX_FILENAME_LENGTH 32
#define MAX_TIMEDATE_LENGTH 32
#define BUFSIZE 256

int main(int argc, char *argv[]) {
    struct tm *tm;
    time_t t;
    char name_buffer[BUFSIZE];
    char dirpath_buffer[2 * BUFSIZE];
    int ch, name_length;
    int fd;
    FILE *filedes;
    if (argc != 3) {
        fprintf(stderr, "Hmm, something's wrong here..\n");
        return EXIT_FAILURE;
    }
    strncpy(name_buffer, argv[2], MAX_FILENAME_LENGTH + 1); //copy filename to name_buffer
    name_buffer[MAX_FILENAME_LENGTH] = '\0';
    strcpy(dirpath_buffer, SUBMISSION_FOLDER); 
    strcat(dirpath_buffer, argv[1]); //copy username to dirpath_buffer
    mkdir(dirpath_buffer, O_CREAT | S_IRWXU);
    strcat(dirpath_buffer, "/");
    strcat(dirpath_buffer, name_buffer);
    fd = open(dirpath_buffer, O_RDONLY);
    if (fd != -1) {
        close(fd);
        name_length = strlen(dirpath_buffer);
        dirpath_buffer[name_length] = '_';
        t = time(NULL);
        tm = localtime(&t);
        strftime(dirpath_buffer + name_length + 1, MAX_TIMEDATE_LENGTH, "%H.%M.%S_%d-%m-%Y", tm);
    }
    filedes = fopen(dirpath_buffer, "w");
    while ((ch = getchar()) != EOF)
        fputc(ch, filedes);
    fclose(filedes);
    printf("%s sent.\n", argv[2]);
    return EXIT_SUCCESS;
}
